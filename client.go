// Copyright 2013 The Gorilla WebSocket Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
)

const (
	writeWait      = 10 * time.Second
	pongWait       = 60 * time.Second
	pingPeriod     = (pongWait * 9) / 10
	maxMessageSize = 512
)

type Client struct {
	id   string
	x    float64
	y    float64
	hub  *Hub
	conn *websocket.Conn
	send chan []byte
}

func (client *Client) readPump() {
	defer func() {
		client.hub.unregister <- client
		client.conn.Close()
	}()
	client.conn.SetReadLimit(maxMessageSize)
	client.conn.SetReadDeadline(time.Now().Add(pongWait))
	client.conn.SetPongHandler(func(string) error { client.conn.SetReadDeadline(time.Now().Add(pongWait)); return nil })
	for {
		_, data, err := client.conn.ReadMessage()
		if err != nil {
			log.Println(err)
			return
		}

		var msg Message
		err = json.Unmarshal(data, &msg)
		if err != nil {
			log.Println(err)
			return
		}

		var text string

		if msg.HelloMessage != nil {
			client.id = msg.Hello
			text = fmt.Sprintf("player <%s> entered the game", msg.Hello)
		} else if msg.ByeMessage != nil {
			text = fmt.Sprintf("player <%s> left the game", msg.Bye)
		} else if msg.UpdateMessage != nil {
			client.x = msg.X
			client.x = msg.Y
			if msg.Stop {
				text = fmt.Sprintf("<%s> stopped", msg.Id)
			} else {
				text = fmt.Sprintf("<%s> moving %s", msg.Id, msg.Direction)
			}

		} else if client.id != "" {
			text = fmt.Sprintf("<%s>: %s\n", client.id, string(data))
		} else {
			text = fmt.Sprintf("<%s>: %s\n", client.conn.RemoteAddr(), string(data))
		}

		log.Printf(text)
		client.hub.broadcast <- data
	}
}

func (client *Client) writePump() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		client.conn.Close()
	}()

	for {
		select {
		case msg, ok := <-client.send:
			client.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				// The hub closed the channel.
				client.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			if err := client.conn.WriteMessage(websocket.TextMessage, msg); err != nil {
				return
			}

		case <-ticker.C:
			client.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := client.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		}
	}
}

func serveWs(hub *Hub, w http.ResponseWriter, r *http.Request) {
	upgrader := websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
		CheckOrigin:     func(r *http.Request) bool { return true },
	}

	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}

	client := &Client{id: "", hub: hub, conn: conn, send: make(chan []byte, 256)}
	hub.register <- client

	go client.writePump()
	go client.readPump()
}
