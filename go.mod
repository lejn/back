module bitbucket.org/lejn/back

go 1.12

require (
	github.com/gorilla/handlers v1.4.1
	github.com/gorilla/mux v1.7.3
	github.com/gorilla/websocket v1.4.0
)
