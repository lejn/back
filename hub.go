// Copyright 2013 The Gorilla WebSocket Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

type Hub struct {
	clients    map[*Client]bool
	broadcast  chan []byte
	update     chan bool
	register   chan *Client
	unregister chan *Client
}

func newHub() *Hub {
	return &Hub{
		broadcast:  make(chan []byte),
		update:     make(chan bool),
		register:   make(chan *Client),
		unregister: make(chan *Client),
		clients:    make(map[*Client]bool),
	}
}

type player struct {
	id string
	x  float64
	y  float64
}

// UpdateResponse contains info about the world
type UpdateResponse struct {
	players []player
}

func (hub *Hub) run() {
	for {
		select {
		case client := <-hub.register:
			hub.clients[client] = true
		case client := <-hub.unregister:
			if _, ok := hub.clients[client]; ok {
				delete(hub.clients, client)
				close(client.send)
			}
		case message := <-hub.broadcast:
			for client := range hub.clients {
				select {
				case client.send <- message:
				default:
					close(client.send)
					delete(hub.clients, client)
				}
			}
		}
	}
}

//func (hub *Hub) updateWorld() {
//	var response UpdateResponse
//	response.players = make([]player, len(hub.clients))
//	for client := range hub.clients {
//		var p player
//		p.id = client.id
//		p.x = client.x
//		p.y = client.y
//		response.players = append(response.players, p)
//	}
//
//	data, _ := json.Marshal(response)
//	hub.broadcast <- data
//}
