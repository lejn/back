package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

var addr = flag.String("addr", ":80", "http service address")

func main() {
	flag.Parse()
	hub := newHub()
	go hub.run()

	router := mux.NewRouter()

	router.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
		if r.Method == "OPTIONS" {
			return
		}
		serveWs(hub, w, r)
	})
	fmt.Println("Server started")
	log.Fatal(http.ListenAndServe(":80", router))
}
