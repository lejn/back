package main

type HelloMessage struct {
	Hello string  `json:"hello"`
}

type ByeMessage struct {
	Bye string `json:"bye"`
}

type UpdateMessage struct {
	Direction string  `json:"direction"`
	X         float64 `json:"x"`
	Y         float64 `json:"y"`
	Update    bool    `json:"update"`
	Stop      bool    `json:"stop"`
}

type Message struct {
	Id string `json:"id"`
	Model float64 `json:"model"`
	*HelloMessage
	*ByeMessage
	*UpdateMessage
}
